import React from "react";
import Button from "../Button";
import './Modal.css'

export interface ModalProps {
    content: string
    acceptButtonText: string
    rejectButtonText: string
    acceptButtonColor: string
    rejectButtonColor: string
    onAccept: () => void
    onReject: () => void
}

const Modal = ({
    content = 'Contenu',
    acceptButtonText = 'Accepter',
    rejectButtonText = 'Refuser',
    acceptButtonColor = '#1ae8b7',
    rejectButtonColor = '#7f57dc',
    onAccept,
    onReject
}: ModalProps) => {
    return(
        <div className="modal-container">
            <section className="modal">
                <p>{ content }</p>
                <div className="buttons-container">
                    <Button
                        label={rejectButtonText}  
                        onClick={onReject}
                        backgroundColor={rejectButtonColor}            
                    />
                    <Button
                        label={acceptButtonText}  
                        onClick={onAccept}
                        backgroundColor={acceptButtonColor}         
                    />
                </div>
            </section>
        </div>
    )
}

export default Modal
