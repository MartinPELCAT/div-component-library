import React from "react";
import "./tag.css"

export interface TagProps {
    color?: string;
    value?: string;
}

const Tag = ({
    value = "Catégorie",
    color = "#ff0000"
}: TagProps) => {

    return (
        <p 
        className={["cat-tag"].join(" ")}
        style={{ backgroundColor: color }}
        >
            {value}
        </p>
    )
}

export default Tag;