import React, {forwardRef, useImperativeHandle, useState} from 'react'
import Toast from '../Toast'
import { ToastProps } from '../Toast/Toast'
import './Toaster.css'

export interface ToasterProps {
    name: string,
    initToast?: ToastProps
}

const Toaster = (props: ToasterProps) => {
    const [toasts, setToasts] = props.initToast ? useState<ToastProps[]>([props.initToast]) : useState<ToastProps[]>([])
    const pushNewToast = (toast: ToastProps) => {
        setToasts([...toasts, toast])
    }
    const removeToast = (toast: ToastProps) => {
        const newToasts = [...toasts]
        newToasts.splice(newToasts.indexOf(toast), 1)
        setToasts(newToasts)
    }
    return (
        <div>
            {toasts.length > 0 &&
            toasts.map((toast: ToastProps) => {
                return <Toast
                    content={toast.content}
                    backgroundColor={toast.backgroundColor}
                    duration={toast.duration}
                    position={toast.position}
                    removeToast={removeToast}
                />
            })}
        </div>

    )
}

export default Toaster
