import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Label, { LabelProps } from "./Label";

export default {
  title: "Components/Form/Label",
  component: Label,
} as Meta;

const Template: Story<LabelProps> = (args) => <Label {...args} />;

export const Default = Template.bind({});
Default.args = { value: "Adresse mail :" };

export const IsRequierd = Template.bind({});
IsRequierd.args = { ...Default.args, requierd: true};

export const isError = Template.bind({});
isError.args = { ...Default.args, isError: true};
